<?php

namespace Drupal\comment_email;

use Drupal\comment\CommentInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Mail\MailFormatHelper;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\user\Entity\User;

class CommentEmailHooks {

  /**
   * Implements hook_entity_insert().
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public static function hookEntityInsert(EntityInterface $entity) {
    if ($entity instanceof NodeInterface && $entity->bundle() === 'forum') {
      static::sendMails('node_added', [
        'node' => $entity,
      ]);
    }
    if ($entity instanceof CommentInterface && $entity->bundle() === 'comment_forum') {
      static::sendMails('comment_added', [
        'comment' => $entity,
        'node' => $entity->getCommentedEntity(),
      ]);
    }
  }

  /**
   * Send Mails.
   *
   * Recipient accounts are gathered via hook_comment_email_recipient_accounts
   *
   * @param $key
   *   The mail key.
   * @param array $parameters
   *   The mail parameters.
   * @param array $data
   *   The token data.
   */
  private static function sendMails($key, array $data) {
    /** @var \Drupal\Core\Mail\MailManagerInterface $emailPluginManager */
    $emailPluginManager = \Drupal::service('plugin.manager.mail');

    /** @var AccountInterface[] $accounts */
    $accounts  = \Drupal::moduleHandler()->invokeAll('comment_email_recipient_accounts', [$data, $key]);
    $accounts  += \Drupal::moduleHandler()->invokeAll("comment_email_{$key}_recipient_accounts", [$data, $key]);
    \Drupal::moduleHandler()->alter(['comment_email_recipient_accounts', "comment_email_{$key}_recipient_accounts"], $accounts, $data, $key);

    foreach ($accounts as $account) {
      $user = User::load($account->id());
      $emailPluginManager->mail('comment_email', $key,
        $account->getEmail(), $account->getPreferredLangcode(), $data + ['user' => $user]);
    }
  }

  public static function hookMail($key, &$message, $params) {
    $tokenOptions = $tOptions = ['langcode' => $message['langcode']];

    switch ($key) {

      case 'node_added':
        $subject = t('[[site:name]] Discussion: [node:title]', [], $tOptions);
        $bodyText = <<<EOD
[node:author] has added: [node:title]
To view the comment or cancel the subscription go to [comment:url]
---
[node:title]
[node:body]
EOD;
        $body = t($bodyText, [], $tOptions);
        break;

      case 'comment_added':
        $subject = t('[[site:name]] Discussion: [node:title]', [], $tOptions);
        $bodyText = <<<EOD
[comment:author] has commented on: [node:title]
To view the comment or cancel the subscription go to [comment:url].
---
[comment:body]
EOD;
        $body = t($bodyText, [], $tOptions);
        break;
    }

    $tokenOptions['callback'] = function (&$replacements, $data, $options, $bubbleable_metadata) {
      array_walk($replacements, function (&$replacement) {
        $replacement = trim(MailFormatHelper::htmlToText($replacement));
      });
    };
    $message['subject'] = \Drupal::token()->replace($subject, $params, $tokenOptions);
    $message['body'] = explode("\n", \Drupal::token()->replace($body, $params, $tokenOptions));
  }

}
