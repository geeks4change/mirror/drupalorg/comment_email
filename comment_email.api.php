<?php
/**
 * Forum Mails API
 */

/**
 * Get recipient accounts.
 *
 * @param mixed[] $data
 *   An array of data for the mail.
 * @param string $key
 *  The mail key.
 *
 * @return \Drupal\Core\Session\AccountInterface[]
 */
function hook_comment_email_recipient_accounts(array $data, $key) {
  if ($key === 'node_added' && isset($data['node'])
    && ($node = $data['node']) && $node instanceof \Drupal\node\NodeInterface) {
    return [$node->getOwner()];
  }
}

/**
 * Get recipient accounts.
 *
 * @param mixed[] $data
 *   An array of data for the mail.
 * @param string $key
 *  The mail key.
 *
 * @return \Drupal\Core\Session\AccountInterface[]
 */
function hook_comment_email_KEY_recipient_accounts(array $data, $key) {
  return [\Drupal\user\Entity\User::load(1)];
}

/**
 * Alter recipient accounts.
 *
 * @param \Drupal\Core\Session\AccountInterface[] $accounts
 *   The accounts to alter.
 * @param mixed[] $data
 *   An array of data for the mail.
 * @param string $key
 *  The mail key.
 */
function hook_comment_email_recipient_accounts_alter(&$accounts, array $data, $key) {
  $accounts[] = \Drupal::currentUser();
}

/**
 * Alter recipient accounts.
 *
 * @param \Drupal\Core\Session\AccountInterface[] $accounts
 *   The accounts to alter.
 * @param mixed[] $data
 *   An array of data for the mail.
 * @param string $key
 *  The mail key.
 */
function hook_comment_email_KEY_recipient_accounts_alter(&$accounts, array $data, $key) {
  foreach ($accounts as $index => $account) {
    if ($account->id() == 1) {
      unset($accounts[$key]);
    }
  }
}
